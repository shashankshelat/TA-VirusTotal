function get_setup_content() {
    // This function returns a JSON translation of the original Setup page

    const content_mapping = {
        "generic": [
            {
                "title": "VirusTotal API Token",
                "description": 
                    "The token for VirusTotal API is required to allow the software to connect to the remote endpoint. " +
                    "It will be stored encrypted using Splunk's default password storage mechanism. A free token will work, " +
                    "however may cause significant performance degradation due to limits imposed on frequency of queries by VirusTotal.",
                "input": {
                    "id": "vt_token_id",
                    "label": "Token",
                    "is_checkbox": false,
                    "type": "password",
                    "value": "",
                    "help": 
                        "Defaults to empty. This TA will not work without this setting. To test whether this was " +
                        "configured successfully, you may use the SPL \" | virustotal \" command in an ad-doc search and " +
                        "check if it completes successfully."
                }
            },{
                "title": "VirusTotal Batching",
                "description":
                    "The \"Max.Batch Size\" argument is dependent on the VirusTotal API key. " +
                    "It tells the TA how many resources (hashes or URLs) should be batched into one REST API query. " +
                    "The higher the number, the better the performance. Note that(officially) the free key only supports 4queries / min, so for the free key the max batch size is 4." +
                    "\nThis option only affects Hash and URL requests. The VirusTotal API does not support batched queries for IPs or Domain names at this time.",
                "input": {
                    "id": "vt_maxbatchsz_id",
                    "label": "Max. Batch Size",
                    "is_checkbox": false,
                    "type": "number",
                    "value": "4",
                    "help":
                        "Defaults to 4. Recommend increasing to improve performance. " +
                        "Recommend not increasing past 100, even with large VT licenses, as this may result in large HTTP requests / responses and increased RAM usage."
                }
            },{
                "title": "VirusTotal Command Timeout",
                "description":
                    "On versions of Splunk lower than 7.1.0, the manual \"Stop\" button for the job does not terminate the " +
                    "custom command. It becomes necessary to manually kill the python process, or restart the search head. " +
                    "This setting primarily exists to safeguard against these \"never-ending\" jobs that use this command." +
                    "\nThe recommended setting for this value differs based on usecase, environment, and VT license. " +
                    "In principal it should be configured to about two times longer than the average observed running time of the command under normal conditions. " +
                    "If a timeout occurs, it will be indicated as a warning in the job interface." +
                    "\nThis setting controls the timeout for the command. If the command has been running for more than X seconds, it will terminate itself. " +
                    "This can be set to 0, to disable the timeout." +
                    "Although this timeout is not likely to be necessary often, it is a good idea to set it as a \"sanity-check\"." +
                    "In case something unexpected goes wrong with the command, this timeout will automatically terminate it after X seconds.",
                "input": {
                    "id": "vt_cmdtout_id",
                    "label": "Command Timeout",
                    "is_checkbox": false,
                    "type": "number",
                    "value": "14400",
                    "help":
                        "Defaults to 14400. This equals 4 hours."
                }
            }
        ],
        "proxy": {
            "description": 
                "At the time of writing, socks5 proxies are not supported due to an old version of the python requests " +
                "library being bundled with Splunk and a missing dependency. If these issues are fixed in future, SOCKS5 "+
                "proxies can be referenced like this: socks5://my.socks5.proxy.internal:1080. \n\n" +
                "If your proxy requires authentication, do NOT specify basic auth here.Not only will this not work, but will store your password in plain text.",
            "inputs": [
                {
                    "uid": "1",
                    "id": "vt_penabled_id",
                    "label": "Enable proxy",
                    "help": "",
                },{
                    "uid": "2",
                    "id": "vt_purl_id",
                    "label": "Proxy URL",
                    "help": "Specify the protocol and URL of your proxy (e.g. <i>http://my.http.proxy.internal:8008</i>)",
                },{
                    "uid": "3",
                    "id": "vt_puname_id",
                    "label": "Username",
                    "help": "Blank usernames are unsupported and would disable proxy authentication",
                },{
                    "uid": "4",
                    "id": "vt_ppwd_id",
                    "label": "Password",
                    "help": "Provide password for proxy authentication (will be stored encrypted)",
                }
            ],
        },
        "vt_functionalities": {
            "title": "VirusTotal Functionalities",
            "description": "The following sections can be enabled/disabled independently of one another. " +
                "For example, if you only want to use the \"Hash\" caching functionality, you will not need to enable IP, domain, or url caching. " +
                "If you want to use all the functionalities, configure all sections.",
            "sections": [
            {
                "id": "1",
                "name": "File Hash Caching",
                "label": "form_section_vt_fhc",
                "content": [
                    {
                        "title": "Cache Autoupdate",
                        "description":
                            "Enable this to allow auto-updating of the lookup-based cache. " +
                            "With this option disabled, the only way to use the TA is by using the ' | virustotal ' " +
                            "command in SPL to perform live lookups against VT API.\n" +
                            "It is recommended that this option be enabled, as using a KVStore-backed lookup is significantly faster and cheaper resource-wise.",
                        "input": {
                            "id": "fhc_enable_id",
                            "label": "Enable Schedule for Update Hash Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    },{
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for refreshing VirusTotal results.",
                        "input": {
                            "id": "fhc_cron_schedule_id",
                            "label": "Update Hash Lookup",
                            "value": "0 6 1 * *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Standard cron format. Default is every 1 months, on the 1st day of the month, at 6 am"
                        },
                    },{
                        "title": "Earliest Time",
                        "description":
                            "This setting is closely related to the cron schedule. It should typically be configured to run over more/less the same time period as the interval between cron executions. " +
                            "\nNote that the default \"Earliest Time\" for this search is slightly longer than it's cron schedule. This is done on purpose to ensure that no data is skipped in the interval.",
                        "input": {
                            "id": "fhc_earliest_time_id",
                            "label": "Earliest Time",
                            "value": "-33d",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is 33 days, to be specified in Splunk's standard time format (e.g. -1d)"
                        },
                    },{
                        "title": "Index Filter",
                        "description":
                            "This filter should capture all events with hashes, and rename the field with the hash to \"hash\". This option is very environment specific." +
                            "\nExample: <i>index = email_filter attachment_hash=* | rename attachment_hash as hash</i>" +
                            "\nIn the above example, all the interesting hashes are located in the 'email_filter' index. " +
                            "The hashes are in a field called 'attachment_hash', which we rename to 'hash' to comply with the TA.",
                        "input": {
                            "id": "fhc_vt_macro_hash_filter_id",
                            "label": "Index Filter",
                            "value": "index=hashes_index | rename \"MD5_Hash\" as hash",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Supported hashes: md5, sha1, sha256"
                        },
                    },{
                        "title": "Refresh Interval",
                        "description":
                            "This is the time interval in which hashes will be re-scanned with VT TA. " +
                            "If more than [value] seconds have elapsed since the VT API was queried for a particular hash, the API will be queried again to see if any new information is available for the hash.",
                        "input": {
                            "id": "fhc_vt_cache_maxage_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*30",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*30s)"
                        },
                    },{
                        "title": "Unknown Hash Refresh Interval",
                        "description":
                            "Performs the same task as the \"Refresh Interval\" above, but applied to \"Unknown Hashes\" (not recognized by VirusTotal). " +
                            "It may be beneficial to have a smaller \"Refresh Interval\" for these hashes, to increase the frequency with which VT is queried for new information concerning those files.",
                        "input": {
                            "id": "fhc_vt_cache_unknown_maxage_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*7",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*7s)"
                        },
                    }
                ] 
            },{
                "id": "2",
                "name": "File Hash Cache Cleanup",
                "label": "form_section_vt_fhcc",
                "content": [
                    {
                        "title": "Cache Autoclean",
                        "description": 
                            "Enable this to allow auto-cleaning of the lookup-based cache. " +
                            "The TA will work without it, however may accumulate large amounts of historical data over time. " +
                            "This search does not touch the index, and is not very performance expensive.",
                        "input": {
                            "id": "fhcc_enable_id",
                            "label": "Enable Schedule for Clean Hash Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    },{
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for running the cleanup command for removal of old hashes from the KVStore. " +
                            "This command is very lightweight, offloading the majority of workload to the backing MongoDB instance.",
                        "input": {
                            "id": "fhcc_cron_schedule_id",
                            "label": "Clean Hash Lookup",
                            "value": "0 6 2 * /3 *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is every 3 months, on the 2nd day of the month, at 6 am"
                        },
                    },{
                        "title": "Max Age",
                        "description":
                            "Cached hashes which have not been seen in more than \"Max Age\" will be removed from the KVStore.",
                        "input": {
                            "id": "fhcc_vt_cache_retention_id",
                            "label": "Max Age Before Refresh",
                            "value": "7890000",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Arithmetic-expressions NOT supported. Default is 3 months (7890000s)"
                        },
                    }
                ]
            },{
                "id": "3",
                "name": "URL Intel Caching",
                "label": "form_section_vt_uic",
                "content": [
                    {
                        "title": "Cache Autoupdate",
                        "description":
                            "Enable this to allow auto-updating of the lookup-based cache. " +
                            "With this option disabled, the only way to use the TA is by using the ' | virustotal ' " +
                            "command in SPL to perform live lookups against VT API.\n" +
                            "It is recommended that this option be enabled, as using a KVStore-backed lookup is significantly faster and cheaper resource-wise.",
                        "input": {
                            "id": "uic_enable_id",
                            "label": "Enable Schedule for Update URL Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    },{
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for refreshing VirusTotal results.",
                        "input": {
                            "id": "uic_cron_schedule_id",
                            "label": "Update URL Lookup",
                            "value": "0 6 1 * *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Standard cron format. Default is every 1 months, on the 1st day of the month, at 6 am"
                        },
                    },{
                        "title": "Earliest Time",
                        "description":
                            "This setting is closely related to the cron schedule. It should typically be configured to run over more/less the same time period as the interval between cron executions. " +
                            "\nNote that the default \"Earliest Time\" for this search is slightly longer than it's cron schedule. This is done on purpose to ensure that no data is skipped in the interval.",
                        "input": {
                            "id": "uic_earliest_time_id",
                            "label": "Earliest Time",
                            "value": "-33d",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is 33 days, to be specified in Splunk's standard time format (e.g. -1d)"
                        },
                    },{
                        "title": "Index Filter",
                        "description":
                            "This filter should capture all events with URLs, and rename the field with the URL to \"url\". " +
                            "This option is very environment specific. This search will be used as the basis for cache creation. " +
                            "The results of this search will be checked against VirusTotal, and the intelligence will be brought back into the local cache. " +
                            "Only items that aren't already present in the cache - or those where the cache entry has expired - will be checked. " +
                            "For best performance - if DMA is enabled on your system - consider using a \"tstats\" search." +
                            "\nExample: <i>index=security_proxy dest_url=* | rename dest_url as url</i>" +
                            "\nIn the above example, all the interesting URLs are located in the 'security_proxy' index. " +
                            "The URLs are in a field called 'dest_url', which we rename to 'url' to comply with the TA.",
                        "input": {
                            "id": "uic_vt_macro_url_filter_id",
                            "label": "Index Filter",
                            "value": "index=my_proxy_logs | rename \"target_url\" as url",
                            "is_checkbox": false,
                            "type": "text",
                            "help": ""
                        },
                    },{
                        "title": "Refresh Interval",
                        "description":
                            "This is the time interval in which URLs will be re-scanned with VT TA. " +
                            "If more than [value] seconds have elapsed since the VT API was queried for intel on a particular URL, the API will be queried again to see if any new information is available for the URL.",
                        "input": {
                            "id": "uic_vt_cache_maxage_url_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*30",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*30s)"
                        },
                    },{
                        "title": "Unknown URL Refresh Interval",
                        "description":
                            "Performs the same task as the \"Refresh Interval\" above, but applied to \"Unknown URLs\" (not recognized by VirusTotal). " +
                            "It may be beneficial to have a smaller \"Refresh Interval\" for these URLs, to increase the frequency with which VT is queried for new information concerning those data points.",
                        "input": {
                            "id": "uic_vt_cache_unknown_maxage_url_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*7",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*7s)"
                        },
                    }
                ] 
            },{
                "id": "4",
                "name": "URL Cache Cleanup",
                "label": "form_section_vt_ucc",
                "content": [
                    {
                        "title": "Cache Autoclean",
                        "description":
                            "Enable this to allow auto-cleaning of the lookup-based cache. " +
                            "The TA will work without it, however may accumulate large amounts of historical data over time. " +
                            "This search does not touch the index, and is not very performance expensive.",
                        "input": {
                            "id": "ucc_enable_id",
                            "label": "Enable Schedule for Clean URL Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    }, {
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for running the cleanup command for removal of old URLs from the KVStore. " +
                            "This command is very lightweight, offloading the majority of workload to the backing MongoDB instance.",
                        "input": {
                            "id": "ucc_cron_schedule_id",
                            "label": "Clean URL Lookup",
                            "value": "0 6 2 * /3 *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is every 3 months, on the 2nd day of the month, at 6 am"
                        },
                    }, {
                        "title": "Max Age",
                        "description":
                            "Cached URLs which have not been seen in more than \"Max Age\" will be removed from the KVStore.",
                        "input": {
                            "id": "ucc_vt_cache_retention_url_id",
                            "label": "Max Age Before Refresh",
                            "value": "7890000",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Arithmetic-expressions NOT supported. Default is 3 months (7890000s)"
                        },
                    }
                ]
            }, {
                "id": "5",
                "name": "Domain Intel Caching",
                "label": "form_section_vt_dic",
                "content": [
                    {
                        "title": "Cache Autoupdate",
                        "description":
                            "Enable this to allow auto-updating of the lookup-based cache. " +
                            "With this option disabled, the only way to use the TA is by using the ' | virustotal ' " +
                            "command in SPL to perform live lookups against VT API.\n" +
                            "It is recommended that this option be enabled, as using a KVStore-backed lookup is significantly faster and cheaper resource-wise.",
                        "input": {
                            "id": "dic_enable_id",
                            "label": "Enable Schedule for Update Domain Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    }, {
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for refreshing VirusTotal results.",
                        "input": {
                            "id": "dic_cron_schedule_id",
                            "label": "Update Domain Lookup",
                            "value": "0 6 1 * *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Standard cron format. Default is every 1 months, on the 1st day of the month, at 6 am"
                        },
                    }, {
                        "title": "Earliest Time",
                        "description":
                            "This setting is closely related to the cron schedule. It should typically be configured to run over more/less the same time period as the interval between cron executions. " +
                            "\nNote that the default \"Earliest Time\" for this search is slightly longer than it's cron schedule. This is done on purpose to ensure that no data is skipped in the interval.",
                        "input": {
                            "id": "dic_earliest_time_id",
                            "label": "Earliest Time",
                            "value": "-33d",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is 33 days, to be specified in Splunk's standard time format (e.g. -1d)"
                        },
                    }, {
                        "title": "Index Filter",
                        "description":
                            "This filter should capture all events with URLs, and rename the field with the URL to \"url\". " +
                            "This option is very environment specific. This search will be used as the basis for cache creation. " +
                            "The results of this search will be checked against VirusTotal, and the intelligence will be brought back into the local cache. " +
                            "Only items that aren't already present in the cache - or those where the cache entry has expired - will be checked. " +
                            "For best performance - if DMA is enabled on your system - consider using a \"tstats\" search." +
                            "\nExample: <i>index=security_proxy dest_url=* | rename site_domain as domain</i>" +
                            "\nIn the above example, all the interesting URLs are located in the 'security_proxy' index. " +
                            "The domains are in a field called 'site_domain', which we rename to 'domain' to comply with the TA.",
                        "input": {
                            "id": "dic_vt_macro_domain_filter_id",
                            "label": "Index Filter",
                            "value": "index=ssl_inspector | rename \"cert_cn\" as domain",
                            "is_checkbox": false,
                            "type": "text",
                            "help": ""
                        },
                    }, {
                        "title": "Refresh Interval",
                        "description":
                            "This is the time interval in which URLs will be re-scanned with VT TA. " +
                            "If more than [value] seconds have elapsed since the VT API was queried for intel on a particular URL, the API will be queried again to see if any new information is available for the URL.",
                        "input": {
                            "id": "dic_vt_cache_maxage_domain_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*30",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*30s)"
                        },
                    }, {
                        "title": "Unknown Domain Refresh Interval",
                        "description":
                            "Performs the same task as the \"Refresh Interval\" above, but applied to \"Unknown Domains\" (not recognized by VirusTotal). " +
                            "It may be beneficial to have a smaller \"Refresh Interval\" for these domains, to increase the frequency with which VT is queried for new information concerning those data points.",
                        "input": {
                            "id": "dic_vt_cache_unknown_maxage_domain_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*7",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*7s)"
                        },
                    }
                ]
            }, {
                "id": "6",
                "name": "Domain Cache Cleanup",
                "label": "form_section_vt_dcc",
                "content": [
                    {
                        "title": "Cache Autoclean",
                        "description":
                            "Enable this to allow auto-cleaning of the lookup-based cache. " +
                            "The TA will work without it, however may accumulate large amounts of historical data over time. " +
                            "This search does not touch the index, and is not very performance expensive.",
                        "input": {
                            "id": "dcc_enable_id",
                            "label": "Enable Schedule for Clean Domain Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    }, {
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for running the cleanup command for removal of old domains from the KVStore. " +
                            "This command is very lightweight, offloading the majority of workload to the backing MongoDB instance.",
                        "input": {
                            "id": "dcc_cron_schedule_id",
                            "label": "Clean Domain Lookup",
                            "value": "0 6 2 * /3 *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is every 3 months, on the 2nd day of the month, at 6 am"
                        },
                    }, {
                        "title": "Max Age",
                        "description":
                            "Cached domains which have not been seen in more than \"Max Age\" will be removed from the KVStore.",
                        "input": {
                            "id": "dcc_vt_cache_retention_domain_id",
                            "label": "Max Age Before Refresh",
                            "value": "7890000",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Arithmetic-expressions NOT supported. Default is 3 months (7890000s)"
                        },
                    }
                ]
            }, {
                "id": "7",
                "name": "IP Intel Caching",
                "label": "form_section_vt_iic",
                "content": [
                    {
                        "title": "Cache Autoupdate",
                        "description":
                            "Enable this to allow auto-updating of the lookup-based cache. " +
                            "With this option disabled, the only way to use the TA is by using the ' | virustotal ' " +
                            "command in SPL to perform live lookups against VT API.\n" +
                            "It is recommended that this option be enabled, as using a KVStore-backed lookup is significantly faster and cheaper resource-wise.",
                        "input": {
                            "id": "iic_enable_id",
                            "label": "Enable Schedule for Update IP Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    }, {
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for refreshing VirusTotal results.",
                        "input": {
                            "id": "iic_cron_schedule_id",
                            "label": "Update IP Lookup",
                            "value": "0 6 1 * *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Standard cron format. Default is every 1 months, on the 1st day of the month, at 6 am"
                        },
                    }, {
                        "title": "Earliest Time",
                        "description":
                            "This setting is closely related to the cron schedule. It should typically be configured to run over more/less the same time period as the interval between cron executions. " +
                            "\nNote that the default \"Earliest Time\" for this search is slightly longer than it's cron schedule. This is done on purpose to ensure that no data is skipped in the interval.",
                        "input": {
                            "id": "iic_earliest_time_id",
                            "label": "Earliest Time",
                            "value": "-33d",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is 33 days, to be specified in Splunk's standard time format (e.g. -1d)"
                        },
                    }, {
                        "title": "Index Filter",
                        "description":
                            "This filter should capture all events with external IPv4 addresses, and rename the field with the IP address to \"ip\". " +
                            "This option is very environment specific. This search will be used as the basis for cache creation. " +
                            "The results of this search will be checked against VirusTotal, and the intelligence will be brought back into the local cache. " +
                            "Only items that aren't already present in the cache - or those where the cache entry has expired - will be checked. " +
                            "For best performance - if DMA is enabled on your system - consider using a \"tstats\" search." +
                            "\nExample: <i>index=firewall dest_ip=* dest=EXTERNAL | rename dest_ip as ip</i>" +
                            "\nIn the above example, all the interesting IPs are located in the 'firewall' index. " +
                            "The IP addresses are in a field called 'dest_ip', which we rename to 'ip' to comply with the TA.",
                        "input": {
                            "id": "iic_vt_macro_ip_filter_id",
                            "label": "Index Filter",
                            "value": "index=flow_logs | rename \"src_address\" as ip",
                            "is_checkbox": false,
                            "type": "text",
                            "help": ""
                        },
                    }, {
                        "title": "Refresh Interval",
                        "description":
                            "This is the time interval in which URLs will be re-scanned with VT TA. " +
                            "If more than [value] seconds have elapsed since the VT API was queried for intel on a particular IP, the API will be queried again to see if any new information is available for the IP.",
                        "input": {
                            "id": "iic_vt_cache_maxage_ip_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*30",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*30s)"
                        },
                    }, {
                        "title": "Unknown Domain Refresh Interval",
                        "description":
                            "Performs the same task as the \"Refresh Interval\" above, but applied to \"Unknown IPs\" (not recognized by VirusTotal). " +
                            "It may be beneficial to have a smaller \"Refresh Interval\" for these IPs, to increase the frequency with which VT is queried for new information concerning those data points.",
                        "input": {
                            "id": "iic_vt_cache_unknown_maxage_ip_id",
                            "label": "Cache Max Age Before Refresh",
                            "value": "60*60*24*7",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Can be eval-based arithmetic expression. Default is 1 month (60*60*24*7s)"
                        },
                    }
                ]
            }, {
                "id": "8",
                "name": "IP Cache Cleanup",
                "label": "form_section_vt_icc",
                "content": [
                    {
                        "title": "Cache Autoclean",
                        "description":
                            "Enable this to allow auto-cleaning of the lookup-based cache. " +
                            "The TA will work without it, however may accumulate large amounts of historical data over time. " +
                            "This search does not touch the index, and is not very performance expensive.",
                        "input": {
                            "id": "icc_enable_id",
                            "label": "Enable Schedule for Clean Domain Lookup",
                            "value": "",
                            "is_checkbox": true,
                            "type": "",
                            "help": ""
                        },
                    }, {
                        "title": "Configure Cron Schedule",
                        "description":
                            "Specify the cron schedule for running the cleanup command for removal of old IPs from the KVStore. " +
                            "This command is very lightweight, offloading the majority of workload to the backing MongoDB instance.",
                        "input": {
                            "id": "icc_cron_schedule_id",
                            "label": "Clean Domain Lookup",
                            "value": "0 6 2 * /3 *",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Default is every 3 months, on the 2nd day of the month, at 6 am"
                        },
                    }, {
                        "title": "Max Age",
                        "description":
                            "Cached IPs which have not been seen in more than \"Max Age\" will be removed from the KVStore." +
                            "\nShould be longer than \"Refresh Interval\" and \"Unknown Refresh Interval\", or may delete items prematurely.",
                        "input": {
                            "id": "icc_vt_cache_retention_ip_id",
                            "label": "Max Age Before Refresh",
                            "value": "7890000",
                            "is_checkbox": false,
                            "type": "text",
                            "help": "Value in seconds. Arithmetic-expressions NOT supported. Default is 3 months (7890000s)"
                        },
                    }
                ]
            }
        ]}
    };
    
    return content_mapping;
}

// ----------------------------------
// Functions to fetch content
// ----------------------------------

function get_generic_data() {
    return get_setup_content().generic;
}

function get_proxy_data() {
    return get_setup_content().proxy;
}

function get_vt_functionalities() {
    return get_setup_content().vt_functionalities;   
}

export { 
    get_generic_data,
    get_proxy_data,
    get_vt_functionalities 
};