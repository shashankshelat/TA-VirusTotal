[VT Cache Health Check]
action.email.useNSSubject = 1
alert.track = 0
description = If "count" is greater than 1 on any hash, this indicates that there is data duplication in the KVStore (this could turn into a physical memory leak). "VT Last Queried TIme" is the last time that the hash was tested against the VT API
dispatch.earliest_time = -24h@h
dispatch.latest_time = now
dispatchAs = user
display.general.timeRangePicker.show = 0
display.general.type = statistics
display.page.search.tab = statistics
display.visualizations.show = 0
request.ui_dispatch_app = ta_vt
request.ui_dispatch_view = search
search = | inputlookup virustotal_hash_cache\
| fields *\
| stats count, max(vt_query_time) as q_time by vt_hashes\
| where len(vt_hashes)=32\
| sort - count\
| eval q_time=strftime(q_time, "%d-%m-%y %H:%M")\
| rename q_time as "VT Last Queried Time"


#
# Section: Hash Searches
#

[VirusTotal Update Hash Lookup]
enableSched = 0
alert.track = 0
cron_schedule = 0 6 1 * *
dispatch.earliest_time = -33d
display.general.type = statistics
display.page.search.tab = statistics
request.ui_dispatch_view = search
search = `vt_macro_hash_filter`\
| fields hash | eval hash=lower(hash) | stats count by hash | fields hash\
| lookup virustotal_hash_cache vt_hashes AS hash OUTPUT vt_classification, vt_query_time\
| eval vt_expired=if(\
(((now()-vt_query_time)<(`vt_cache_unknown_maxage`)) AND vt_classification="unknown_hash") OR\
(((now()-vt_query_time)<(`vt_cache_maxage`)) AND vt_classification!="unknown_hash"), 0, 1)\
| search NOT vt_classification=* OR vt_expired=1\
| virustotal hash=hash\
| search vt_resource=*\
| table vt_resource, vt_query_time, vt_classification, vt_scan_date, vt_permalink, vt_positives, vt_total, vt_threat_av, vt_threat_id, vt_hashes\
| eval _key=vt_resource\
| outputlookup append=t key_field=_key virustotal_hash_cache

[VirusTotal Clean Hash Lookup]
enableSched = 0
description = Removes old entries from the lookup collection
alert.track = 0
cron_schedule = 0 6 2 */3 *
dispatch.earliest_time = -7d@h
dispatch.latest_time = now
display.general.timeRangePicker.show = 0
display.page.search.mode = verbose
display.visualizations.show = 0
request.ui_dispatch_app = search
request.ui_dispatch_view = search
schedule_window = auto
search = | cleantimekv collection=virustotal_hash_cache timestamp_field=vt_query_time maxage=`vt_cache_retention`



#
# Section: URL Searches
#

[VirusTotal Update URL Lookup]
enableSched = 0
alert.track = 0
cron_schedule = 0 6 1 * *
dispatch.earliest_time = -33d
display.general.type = statistics
display.page.search.tab = statistics
request.ui_dispatch_view = search
search = `vt_macro_url_filter`\
| fields url | stats count by url | fields url\
| lookup virustotal_url_cache vt_urls AS url OUTPUT vt_classification, vt_query_time\
| eval vt_expired=if(\
(((now()-vt_query_time)<(`vt_cache_unknown_maxage_url`)) AND vt_classification="unknown_url") OR\
(((now()-vt_query_time)<(`vt_cache_maxage_url`)) AND vt_classification!="unknown_url"), 0, 1)\
| search NOT vt_classification=* OR vt_expired=1\
| virustotal url=url\
| search vt_resource=*\
| table vt_resource, vt_*\
| eval _key=vt_resource\
| outputlookup append=t key_field=_key virustotal_url_cache

[VirusTotal Clean URL Lookup]
enableSched = 0
description = Removes old entries from the URL lookup collection
alert.track = 0
cron_schedule = 0 6 2 */3 *
dispatch.earliest_time = -7d@h
dispatch.latest_time = now
display.general.timeRangePicker.show = 0
display.page.search.mode = verbose
display.visualizations.show = 0
request.ui_dispatch_app = search
request.ui_dispatch_view = search
schedule_window = auto
search = | cleantimekv collection=virustotal_url_cache timestamp_field=vt_query_time maxage=`vt_cache_retention_url`



#
# Section: Domain Searches
#

[VirusTotal Update Domain Lookup]
enableSched = 0
alert.track = 0
cron_schedule = 0 6 1 * *
dispatch.earliest_time = -33d
display.general.type = statistics
display.page.search.tab = statistics
request.ui_dispatch_view = search
search = `vt_macro_domain_filter`\
| fields domain | eval domain=lower(domain) | stats count by domain | fields domain\
| lookup virustotal_domain_cache vt_resource AS domain OUTPUT vt_classification, vt_query_time\
| eval vt_expired=if(\
(((now()-vt_query_time)<(`vt_cache_unknown_maxage_domain`)) AND vt_classification="unknown_domain") OR\
(((now()-vt_query_time)<(`vt_cache_maxage_domain`)) AND vt_classification!="unknown_domain"), 0, 1)\
| search NOT vt_classification=* OR vt_expired=1\
| virustotal domain=domain\
| search vt_resource=*\
| table vt_resource, vt_*\
| eval _key=vt_resource\
| outputlookup append=t key_field=_key virustotal_domain_cache

[VirusTotal Clean Domain Lookup]
enableSched = 0
description = Removes old entries from the lookup collection
alert.track = 0
cron_schedule = 0 6 2 */3 *
dispatch.earliest_time = -7d@h
dispatch.latest_time = now
display.general.timeRangePicker.show = 0
display.page.search.mode = verbose
display.visualizations.show = 0
request.ui_dispatch_app = search
request.ui_dispatch_view = search
schedule_window = auto
search = | cleantimekv collection=virustotal_domain_cache timestamp_field=vt_query_time maxage=`vt_cache_retention_domain`


#
# Section: IP Searches
#

[VirusTotal Update IP Lookup]
enableSched = 0
alert.track = 0
cron_schedule = 0 6 1 * *
dispatch.earliest_time = -33d
display.general.type = statistics
display.page.search.tab = statistics
request.ui_dispatch_view = search
search = `vt_macro_ip_filter`\
| fields ip | eval ip=lower(ip) | stats count by ip | fields ip\
| lookup virustotal_ip_cache vt_resource AS ip OUTPUT vt_classification, vt_query_time\
| eval vt_expired=if(\
(((now()-vt_query_time)<(`vt_cache_unknown_maxage_ip`)) AND vt_classification="unknown_ip") OR\
(((now()-vt_query_time)<(`vt_cache_maxage_ip`)) AND vt_classification!="unknown_ip"), 0, 1)\
| search NOT vt_classification=* OR vt_expired=1\
| virustotal ip=ip\
| search vt_resource=*\
| table vt_resource, vt_*\
| eval _key=vt_resource\
| outputlookup append=t key_field=_key virustotal_ip_cache

[VirusTotal Clean IP Lookup]
enableSched = 0
description = Removes old entries from the lookup collection
alert.track = 0
cron_schedule = 0 6 2 */3 *
dispatch.earliest_time = -7d@h
dispatch.latest_time = now
display.general.timeRangePicker.show = 0
display.page.search.mode = verbose
display.visualizations.show = 0
request.ui_dispatch_app = search
request.ui_dispatch_view = search
schedule_window = auto
search = | cleantimekv collection=virustotal_ip_cache timestamp_field=vt_query_time maxage=`vt_cache_retention_ip`