[cleantimekv-command]
syntax = cleantimekv (<cleantimekv-options>)*
shortdesc = Remove items from KVStore based on age
description = Uses a timestamp in a KVStore collection to remove entries older than "age".
usage = public
example1 = cleantimekv collection=virustotal_hash_cache timestamp_field=vt_query_time maxage=86400
comment1 = Removes all entries older than 1 day from collection "virustotal_hash_cache", using the collection's "vt_query_time" field as the unix timestamp.
tags = delete remove kvstore

[cleantimekv-options]
syntax = collection=<str> | timestamp_field=<str>| maxage=<int>
description = The 'collection' param should be set to the name of the collection to be affected.\
The 'timestamp_field' param should be set to the name of a field within the collection. This field should contain the unix timestamp for the event.\
The 'maxage' param should be set to the number of seconds representing the max age to retain events.\
For example, if maxage=86400, this command will *remove* all events from the 'collection' where 'timestamp_field'<now()-maxage.

[virustotal-command]
syntax = virustotal (<virustotal-options>)*
shortdesc = Query VirusTotal API to get information about hashes
description = Gets information from VirusTotal by querying the API for information about the hash found in the field specified in the 'hash' param.
usage = public
example1 = virustotal hash=MD5_Hash
comment1 = Gets information from VirusTotal, querying based on the md5, sha256, or sha1 hash found in the events' MD5_Hash field
tags = lookup virustotal hash

[virustotal-options]
syntax = hash=<field> | ip=<field> | url=<field> | domain=<field> | mode=<"v1"|"json"> | rescan=<bool>
description = The hash param is used to define which field holds the hash to be queried against VT.\
The rescan field, if set to false, will not query VT API for rows that already have a "vt_resource" field set.\
As such, the command will then only supplement data, instead of querying VT API for all of it.
